# Word embeddings et deep learning pour la segmentation automatique de textes et l’extraction d’informations géographiques

## Description

Ce repository regroupe les notebooks et les données d'un travail sur la classification de phrases mené dans le cadre du projet [HextGEO](https://www.insa-lyon.fr/fr/hextgeo).
Ce travail a été réalisé par Hussam Ghanem dans le cadre de son stage de fin d'étude de Master 2 Informatique du l'Université de Strasbourg.


L’objectif général de ce projet est de développer une méthode adaptée pour l’extraction d’informations géo-sémantiques issues de documents textuels (romans). 
L’hétérogénéité de ces documents et l’ambiguïté de la langue est un verrou important afin d’appliquer les méthodes d’extraction automatique d’informations sur des données homogènes et génériques, l’hypothèse était de segmenter le texte afin de faire un premier filtre et de repérer les parties du texte qui contiennent des informations géographiques. 
Nous proposons dans ce travail de faire filtre en effectuant une méthode de classification automatique de phrases (segmentation de texte) afin d'identifier les parties du textes qui seront pertinentes d'analyser et avec comme objectif de réduire la reconnaissance de faux positifs.
Notre méthode est composée de deux étapes principales. La première a pour objectif la construction automatique d’un corpus labellisé. Ce corpus de phrases labellisées servira de jeu de données d’entraînement et d’évaluation pour la phase de classification supervisée. La deuxième étape de notre proposition concerne donc l’entraînement supervisé d’un modèle de classification. Notre objectif est d’identifier de manière automatique les phrases qui comportent des informations géographiques du type expression de déplacement associé à la présence d’un nom de lieu.

Notre méthode prend comme entrée des romans au format TXT ainsi que des fichiers XML contenant des annotations sémantiques réalisées de manière automatique par l’outil PERDIDO.

![segmentation_de_texte](/uploads/2e46368fb7caf0a5df1a3a41b7e3f63b/segmentation_de_texte.png)




### Labellisation automatique d’un corpus

Il s’agit de créer un jeu de données composé de deux parties : une pour l'entraînement et l’autre pour l’évaluation. Ce jeu de données contient deux colonnes : sentences, labels. Ces deux colonnes indiquent des phrases et des labels qui permettent de savoir si les phrases sont des indications géographiques ou non (label 0 : pas géographique, label 1 :  géographique)


![labellisation_de_corpus](/uploads/4061ec057c8d846693b1ba088c68527a/labellisation_de_corpus.png)




### Classification de phrases

Cette étape prend comme entrée le corpus labellisés et généré par l’étape précédent. Dans cette étape, nous créons et entraînons notre modèle à partir d’un modèle pré-entraîné Multilinguel de BERT. 



![classification](/uploads/71a28d2f145d3a5752c871656d75b63c/classification.png)





## Execution

Vous trouver dans les notebooks les commentaires utiles pour comprendre mieux la méthode. Vous pouvez exectuer ces notebooks en utilisant Google Colab. De plus, vous trouverez dans le dossier Data le corpus (les romans) de première étape ainsi que le corpus labellisé généré par la première étape qui est utilisé pour entraîner et évaluer le modèle dans la deuxième étape. 


## Remerciement

Ce travail a été réalisé grâce au soutien financier du LABEX IMU (ANR-10-LABX-0088) et du Projet IDEXLYON de l’Université de Lyon, dans le cadre du programme « Investissements d’Avenir » (ANR-11-IDEX-0007) et (ANR-16-IDEX-0005).



